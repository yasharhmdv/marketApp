package com.example.marketAPP.dto;


import jakarta.persistence.Entity;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BranchRequest {
    private String branchName;
    private Integer countOfEmployee;
}

package com.example.marketAPP.dto;


import com.example.marketAPP.entity.Address;
import jakarta.persistence.OneToOne;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BranchResponse {


    private Integer id;
    private String branchName;
    private Integer countOfEmployee;
    private Address address;

}

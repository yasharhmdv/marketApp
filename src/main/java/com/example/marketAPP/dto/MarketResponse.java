package com.example.marketAPP.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketResponse {
    private Integer id;
    private String marketName;
    private String type;
    private List<BranchResponse> branchesList = new ArrayList<>();

}

package com.example.marketAPP.dto;

import lombok.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketRequest {
    private String marketName;
    private String type;
}

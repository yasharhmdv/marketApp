package com.example.marketAPP.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressRequest {

    private Integer id;
    private String addressName;
}

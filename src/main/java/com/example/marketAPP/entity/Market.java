package com.example.marketAPP.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "market")
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private String marketName;
    private String type;

    @OneToMany(mappedBy = "market", cascade = CascadeType.MERGE)
    private List<Branch> branchList= new ArrayList<>();

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "market_region",
            joinColumns = @JoinColumn(name = "market_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id"))
    List<Region> regions = new ArrayList<>();

}

package com.example.marketAPP.repository;

import com.example.marketAPP.entity.Market;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Integer> {


}

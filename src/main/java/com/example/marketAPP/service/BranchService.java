package com.example.marketAPP.service;

import com.example.marketAPP.dto.BranchRequest;
import com.example.marketAPP.dto.BranchResponse;
import com.example.marketAPP.entity.Branch;
import com.example.marketAPP.entity.Market;
import com.example.marketAPP.repository.BranchRepository;
import com.example.marketAPP.repository.MarketRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BranchService {
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public BranchService(BranchRepository branchRepository, MarketRepository marketRepository, ModelMapper modelMapper) {
        this.branchRepository = branchRepository;
        this.marketRepository = marketRepository;
        this.modelMapper = modelMapper;
    }


    public BranchResponse create(Integer marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);

        branch.setMarket(market);
        market.getBranchList().add(branch);
        branchRepository.save(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    public BranchResponse update(Integer marketId, Integer branchId, BranchRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Market market = marketRepository.findById(marketId).orElseThrow(()->
                new RuntimeException(String.format("Market with id %s not found",marketId)));
        branch = modelMapper.map(request, Branch.class);
        branch.setBranchName(request.getBranchName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        branchRepository.save(branch);
        return modelMapper.map(branch, BranchResponse.class);
    }
    public BranchResponse get(Integer branchId){
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));

        return modelMapper.map(branch,BranchResponse.class);
    }


    public void delete(Integer id) {
        Branch branch = branchRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", id)));
        branchRepository.deleteById(id);
    }

    public List<BranchResponse> getAll() {
        List<Branch> branchList = branchRepository.findAll();
        List<BranchResponse> branchResponseList = new ArrayList<>();
        branchList.forEach(branch ->
                branchResponseList.add(modelMapper.map(branch,BranchResponse.class)));
        return branchResponseList;
    }
}

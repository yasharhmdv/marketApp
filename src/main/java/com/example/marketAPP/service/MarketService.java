package com.example.marketAPP.service;

import com.example.marketAPP.dto.MarketRequest;
import com.example.marketAPP.dto.MarketResponse;
import com.example.marketAPP.entity.Market;
import com.example.marketAPP.entity.Region;
import com.example.marketAPP.repository.MarketRepository;
import com.example.marketAPP.repository.RegionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class MarketService {
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;
    private final RegionRepository regionRepository;

    public MarketService(MarketRepository marketRepository, ModelMapper modelMapper, RegionRepository regionRepository) {
        this.marketRepository = marketRepository;
        this.modelMapper = modelMapper;
        this.regionRepository = regionRepository;
    }

    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request,Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market,MarketResponse.class);
    }


    @Transactional
    public MarketResponse get(Integer marketId) {
        Market market = marketRepository.findById(marketId).orElseThrow(()->
                new RuntimeException(String.format("Market with id %s not found",marketId)));

        return modelMapper.map(market, MarketResponse.class);
    }


    public MarketResponse update(Integer marketId, MarketRequest request){

        Market market = marketRepository.findById(marketId).orElseThrow(()->
                new RuntimeException(String.format("Market with id %s not found",marketId)));
        market = modelMapper.map(request,Market.class);
        market.setMarketName(request.getMarketName());
        market.setType(request.getType());
        marketRepository.save(market);
        return modelMapper.map(market,MarketResponse.class);

    }

    public void delete(Integer marketId) {
        Market market = marketRepository.findById(marketId).orElseThrow(()->
                new RuntimeException(String.format("Market with id %s not found",marketId)));
        marketRepository.deleteById(marketId);
    }

    public Market assignMarketToRegion(Integer marketId, Integer regionId) {

        List<Region> regionList = null;
        Market market = marketRepository.findById(marketId).get();
        Region region = regionRepository.findById(regionId).get();
        regionList = market.getRegions();
        regionList.add(region);
        market.setRegions(regionList);
        return marketRepository.save(market);
    }

    public List<MarketResponse> getAll() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketResponse> marketResponseList = new ArrayList<>();

        marketList.forEach(market ->
                marketResponseList.add(modelMapper.map(market,MarketResponse.class)));
        return marketResponseList;

    }
}

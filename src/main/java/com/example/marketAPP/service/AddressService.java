package com.example.marketAPP.service;

import com.example.marketAPP.dto.AddressRequest;
import com.example.marketAPP.dto.AddressResponse;
import com.example.marketAPP.entity.Address;
import com.example.marketAPP.entity.Branch;
import com.example.marketAPP.repository.AddressRepository;
import com.example.marketAPP.repository.BranchRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Optional;

@Service
public class AddressService {
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;
    private final BranchRepository branchRepository;

    public AddressService(AddressRepository addressRepository, ModelMapper modelMapper,BranchRepository branchRepository) {
        this.addressRepository = addressRepository;
        this.modelMapper = modelMapper;
        this.branchRepository = branchRepository;
    }
    public AddressResponse create(Integer branchId, AddressRequest request){

       Branch branch = branchRepository.findById(branchId).orElseThrow(()->
                new RuntimeException(String.format("Branch with id %s not find",branchId)));

       Address address = modelMapper.map(request, Address.class);
       branch.setAddress(address);
       addressRepository.save(address);
       branchRepository.save(branch);
       return modelMapper.map(address,AddressResponse.class);
    }

    public AddressResponse update(Integer branchId, Integer addressId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(()->
                new RuntimeException(String.format("Branch with id %s not find",branchId)));
        Address address = addressRepository.findById(addressId).orElseThrow(()->
                new RuntimeException(String.format("Address with id %s not find",addressId)));
        address = modelMapper.map(request,Address.class);
        address.setAddressName(request.getAddressName());
        addressRepository.save(address);
        return modelMapper.map(address,AddressResponse.class);

    }

    public AddressResponse get(Integer id) {
        Address address = addressRepository.findById(id).orElseThrow(()->
                new RuntimeException(String.format("Address with id %s not found",id)));
        return modelMapper.map(address,AddressResponse.class);
    }


    public void delete(Integer branchId, Integer addressId) {

        Branch branch = branchRepository.findById(branchId).orElseThrow(()->
                new RuntimeException(String.format("Address with id %s not found",branchId)));
        branch.setAddress(null);
        branchRepository.save(branch);
        addressRepository.deleteById(addressId);
    }
}

package com.example.marketAPP.service;

import com.example.marketAPP.dto.RegionRequest;
import com.example.marketAPP.dto.RegionResponse;
import com.example.marketAPP.entity.Branch;
import com.example.marketAPP.entity.Market;
import com.example.marketAPP.entity.Region;
import com.example.marketAPP.repository.MarketRepository;
import com.example.marketAPP.repository.RegionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegionService {
    private final ModelMapper modelMapper;
    private final RegionRepository regionRepository;
    private final MarketRepository marketRepository;

    public RegionService(ModelMapper modelMapper, RegionRepository regionRepository,
                          MarketRepository marketRepository) {
        this.modelMapper = modelMapper;
        this.regionRepository = regionRepository;
        this.marketRepository = marketRepository;

    }

    public RegionResponse create( RegionRequest request) {
        /*Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));*/
        Region region = modelMapper.map(request,Region.class);

        regionRepository.save(region);

        return modelMapper.map(region,RegionResponse.class);

    }

    public RegionResponse update(Integer regionId,RegionRequest request) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(()->new RuntimeException(String.format("There is no region with %s id",regionId)));
        region = modelMapper.map(request,Region.class);
        region = regionRepository.getById(regionId);
        region.setRegionName(request.getRegionName());
        regionRepository.save(region);
        return modelMapper.map(region,RegionResponse.class);
    }

    public RegionResponse get(Integer regionId) {

        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id: %s not found", regionId)));
        return modelMapper.map(region,RegionResponse.class);
    }

    public void delete(Integer regionId, Integer marketId) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() ->
                        new RuntimeException(String.format("Region with id: %s not found", regionId)));
        Market market = marketRepository.findById(marketId)
                        .orElseThrow(()-> new RuntimeException(String.format("Market with id %s not found",marketId)));
        market.setRegions(null);
        regionRepository.deleteById(regionId);
    }

    public List<RegionResponse> getAll() {
        List<Region> regionList = regionRepository.findAll();
        List<RegionResponse> regionResponseList = new ArrayList<>();
        regionList.forEach(region ->
                regionResponseList.add(modelMapper.map(region,RegionResponse.class)));
        return regionResponseList;
    }

}

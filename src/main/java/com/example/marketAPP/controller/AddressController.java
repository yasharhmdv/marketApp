package com.example.marketAPP.controller;

import com.example.marketAPP.dto.AddressRequest;
import com.example.marketAPP.dto.AddressResponse;
import com.example.marketAPP.service.AddressService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }
    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Integer branchId, @RequestBody AddressRequest request){
        return addressService.create(branchId, request);
    }

    @PutMapping("/{branchId}/address/{addressId}")
    public AddressResponse update(@PathVariable Integer branchId,Integer addressId,@RequestBody AddressRequest request ){

        return addressService.update(branchId,addressId,request);
    }
    @GetMapping("/address/{addressId}")
    public AddressResponse get(@PathVariable Integer id){
        return addressService.get(id);
    }
    @DeleteMapping("/{branchId}/address/{addressId}")
    public void delete(@PathVariable Integer branchId,@PathVariable Integer addressId){
        addressService.delete(branchId,addressId);
    }
}

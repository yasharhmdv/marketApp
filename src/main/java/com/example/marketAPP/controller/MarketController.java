package com.example.marketAPP.controller;

import com.example.marketAPP.dto.MarketRequest;
import com.example.marketAPP.dto.MarketResponse;
import com.example.marketAPP.entity.Market;
import com.example.marketAPP.service.MarketService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
public class MarketController {

    private final MarketService marketService;

    public MarketController(MarketService marketService) {
        this.marketService = marketService;
    }
    @PostMapping
    public MarketResponse save(@RequestBody MarketRequest request){
        return marketService.create(request);
    }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Integer marketId){
        return marketService.get(marketId);
    }

    @GetMapping
    public List<MarketResponse> getAll(){
        return marketService.getAll();
    }

    @PutMapping("/{marketId}")
    public MarketResponse update(@PathVariable Integer marketId, @RequestBody MarketRequest request){
        return marketService.update(marketId,request);
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Integer marketId){
        marketService.delete(marketId);
    }

    @PutMapping("/{marketId}/region/{regionId}")
    public Market assignMarketToRegion(@PathVariable Integer marketId,
                                       @PathVariable Integer regionId){
        return marketService.assignMarketToRegion(marketId,regionId);
    }
}

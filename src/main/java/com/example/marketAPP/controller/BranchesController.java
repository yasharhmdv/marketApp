package com.example.marketAPP.controller;

import com.example.marketAPP.dto.BranchRequest;
import com.example.marketAPP.dto.BranchResponse;
import com.example.marketAPP.service.BranchService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
public class BranchesController {
    private final BranchService branchService;

    public BranchesController(BranchService branchService) {
        this.branchService = branchService;
    }
    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Integer marketId, @RequestBody BranchRequest request){
        return branchService.create(marketId, request);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse update(@PathVariable Integer marketId,
                                 @PathVariable Integer branchId,
                                 @RequestBody BranchRequest request){
        return branchService.update(marketId, branchId, request);
    }
    @GetMapping("/branch/{branchId}")
    public BranchResponse get(@PathVariable Integer branchId){
        return branchService.get(branchId);
    }

    @GetMapping("/branch")
    public List<BranchResponse> getAll(){
        return branchService.getAll();
    }


    @DeleteMapping("branch/{branchId}")
    public void delete(@PathVariable Integer branchId){
        branchService.delete(branchId);
    }
}

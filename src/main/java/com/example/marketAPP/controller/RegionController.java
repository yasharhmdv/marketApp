package com.example.marketAPP.controller;

import com.example.marketAPP.dto.RegionRequest;
import com.example.marketAPP.dto.RegionResponse;
import com.example.marketAPP.entity.Region;
import com.example.marketAPP.repository.RegionRepository;
import com.example.marketAPP.service.RegionService;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/region")
public class RegionController {

    private final RegionService regionService;

    public RegionController(RegionService regionService) {

        this.regionService = regionService;
    }

    @PostMapping
    public RegionResponse create(@RequestBody RegionRequest request){
        return regionService.create(request);

    }
    @PutMapping("/{regionId}")
    public RegionResponse update(@PathVariable Integer regionId,@RequestBody RegionRequest request){
        return regionService.update(regionId,request);
    }
    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Integer regionId){
        return regionService.get(regionId);
    }
    @GetMapping
    public List<RegionResponse> getAll(){
        return regionService.getAll();
    }
    @DeleteMapping("/{regionId}/market/{marketId}")
    public void delete(@PathVariable Integer regionId, @PathVariable Integer marketId){
        regionService.delete(regionId,marketId);
    }
}

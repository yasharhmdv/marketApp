package com.example.marketAPP;

import com.example.marketAPP.repository.AddressRepository;
import com.example.marketAPP.repository.BranchRepository;
import com.example.marketAPP.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MarketAppApplication
{

	public static void main(String[] args) {
		SpringApplication.run(MarketAppApplication.class, args);
	}


}
